create table sj_association_info
(
    association_id      int auto_increment comment '主键编号'
        primary key,
    association_name    varchar(255) null comment '协会名称',
    association_content varchar(999) null comment '协会介绍',
    association_img     varchar(255) null comment '协会照片',
    association_address varchar(255) null comment '协会地址',
    association_status  varchar(255) null comment '状态：1.申请中  2.同意  3.拒绝  4.删除'
);

create table sj_activity_info
(
    activity_id               int auto_increment comment '主键编号'
        primary key,
    activity_name             varchar(255) not null comment '活动名称',
    activity_content          varchar(255) not null comment '活动介绍',
    activity_start_time       datetime     not null comment '开始时间',
    activity_end_time         datetime     not null comment '结束时间',
    activity_address          varchar(255) not null comment '举办地点',
    activity_association_name varchar(255) not null comment '协会名称',
    activity_association_id   int          not null comment '协会id',
    activity_school           varchar(255) null comment '学校名称',
    constraint sj_activity_info_sj_association_info_association_id_fk
        foreign key (activity_association_id) references sj_association_info (association_id)
);

create table sj_comments_activity_info
(
    comments_id           int auto_increment comment '主键标号'
        primary key,
    comments_user_account varchar(255) not null comment '评论用户',
    comments_user_name    varchar(255) not null comment '用户姓名',
    comments_content      varchar(255) not null comment '评论内容',
    comments_create_date  datetime     not null comment '评论时间',
    comments_activity_id  int          not null comment '活动id',
    constraint sj_comments_activity_info_sj_activity_info_activity_id_fk
        foreign key (comments_activity_id) references sj_activity_info (activity_id)
)
    comment '评论活动';

create table sj_comments_association_info
(
    comments_id             int auto_increment comment '主键标号'
        primary key,
    comments_user_account   varchar(255) null comment '评论用户',
    comments_user_name      varchar(255) null comment '用户姓名',
    comments_content        varchar(255) null comment '评论内容',
    comments_create_date    varchar(255) null comment '评论时间',
    comments_association_id int          null comment '协会id'
)
    comment '评论协会';

create table sj_user_activity_info
(
    user_activity_id            int auto_increment comment '主键'
        primary key,
    user_activity_user_account  varchar(255) null comment '账号',
    user_activity_user_name     varchar(255) null comment '姓名',
    user_activity_activity_id   int          null comment '活动id',
    user_activity_activity_name varchar(255) null comment '活动名称',
    user_activity_status        varchar(255) null comment '状态 1申请中  2.同意  3.拒绝'
)
    comment '用户活动';

create table sj_user_association_info
(
    user_association_id               int auto_increment comment '主键'
        primary key,
    user_association_user_account     varchar(255) null comment '账号',
    user_association_user_name        varchar(255) null comment '名称',
    user_association_association_id   int          null comment '协会id',
    user_association_association_name varchar(255) null comment '协会名称',
    user_association_status           int          null comment '状态 1申请中  2.同意  3.拒绝'
)
    comment '用户协会';

create table sj_user_info
(
    user_id             int auto_increment comment '主键'
        primary key,
    user_account        varchar(255) null comment '账号',
    user_pwd            varchar(255) null comment '密码',
    user_img            varchar(255) null comment '头像',
    user_name           varchar(255) null comment '姓名',
    user_association_id int          null comment '协会id',
    user_phone          varchar(255) null comment '手机号',
    user_email          varchar(255) null comment '邮箱',
    user_school         varchar(255) null comment '学校',
    user_role           varchar(255) null comment '角色 1.管理员  2.普通用户',
    constraint sj_admin_sj_association_info_association_id_fk
        foreign key (user_association_id) references sj_association_info (association_id)
)
    comment '用户';

create table sj_user_activity
(
    user_id     int not null,
    activity_id int not null,
    constraint sj_user_activity_sj_activity_info_activity_id_fk
        foreign key (activity_id) references sj_activity_info (activity_id),
    constraint sj_user_activity_sj_user_info_user_id_fk
        foreign key (user_id) references sj_user_info (user_id)
);

create table sj_user_association
(
    user_id        int not null comment 'user id',
    association_id int null comment '协会id',
    constraint sj_user_association_sj_association_info_association_id_fk
        foreign key (association_id) references sj_association_info (association_id),
    constraint sj_user_association_sj_user_info_user_id_fk
        foreign key (user_id) references sj_user_info (user_id)
)
    comment 'user&association';

