package com.itSession.cn.util;

import org.springframework.util.StringUtils;

import java.io.File;
import java.util.Random;
import java.util.UUID;

public class Tools {


    /**
     * 判断字符串是否为空
     *
     * @param str
     * @return
     */
    public static boolean isStrNotNull(String str) {
        if (null != str && str.trim().length() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean createFilePath(String filePath) {
        if (!isStrNotNull(filePath)) {
            //	logger.error("创建文件路径失败，路径为空");
            return false;
        }
        File file2 = new File(filePath);
        // 获取文件所在目录
        String parentPath = file2.getParent();
        File file = new File(parentPath);
        if (isStrNotNull(parentPath)) {
            //文件夹是否存在
            if (!file.exists()) {
                if (file.mkdirs()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            //	logger.error("创建文件路径失败，获取父级");
            return false;
        }
    }

    public static String getUUID32() {
        return UUID.randomUUID().toString().replace("-", "").toLowerCase();
    }

	 public static String getOpenID(String prefix) {
        int first = new Random(10).nextInt(8) + 1;
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        //有可能是负数
        if (hashCodeV < 0) {
            hashCodeV = -hashCodeV;
        }
        String openID = "";
        // 0 代表前面补充0
        // 4 代表长度为4
        // d 代表参数为正数型
        if (!StringUtils.isEmpty(prefix)) {
            openID = prefix + first + String.format("%015d", hashCodeV);
        } else {
            openID = "jh" + first + String.format("%015d", hashCodeV);
        }
        return openID;
    }
}
