package com.itSession.cn.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @ClassName: AssociationInfo
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssociationInfo {
	/**
	 * 主键编号
	 */
	@ApiModelProperty(value = "主键编号")
	private Integer associationId;
	/**
	 * 协会名称
	 */
	@ApiModelProperty(value = "协会名称")
	private String associationName;
	/**
	 * 协会介绍
	 */
	@ApiModelProperty(value = "协会介绍")
	private String associationContent;
	/**
	 * 协会照片
	 */
	@ApiModelProperty(value = "协会照片")
	private String associationImg;
	/**
	 * 协会地址
	 */
	@ApiModelProperty(value = "协会地址")
	private String associationAddress;
	/**
	 * 状态：1.申请中  2.同意  3.拒绝  4.删除
	 */
	@ApiModelProperty(value = "状态：1.申请中  2.同意  3.拒绝  4.删除")
	private String associationStatus;

	private String school;


}