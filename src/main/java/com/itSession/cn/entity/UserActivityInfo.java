package com.itSession.cn.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: UserActivityInfo
 * @description: 用户活动
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserActivityInfo {
	/** 主键 */
    @ApiModelProperty(value = "主键")
	private Integer userActivityId;
	/** 账号 */
    @ApiModelProperty(value = "账号")
	private String userActivityUserAccount;
	/** 姓名 */
    @ApiModelProperty(value = "姓名")
	private String userActivityUserName;
	/** 活动id */
    @ApiModelProperty(value = "活动id")
	private Integer userActivityActivityId;
	/** 活动名称 */
    @ApiModelProperty(value = "活动名称")
	private String userActivityActivityName;
	/** 状态 1申请中  2.同意  3.拒绝 */
    @ApiModelProperty(value = "状态 1申请中  2.同意  3.拒绝")
	private String userActivityStstue;

	
}