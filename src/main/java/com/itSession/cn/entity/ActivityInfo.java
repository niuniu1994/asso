package com.itSession.cn.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: ActivityInfo
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityInfo {
	/** 主键编号 */
    @ApiModelProperty(value = "主键编号")
	private Integer activityId;
	/** 活动名称 */
    @ApiModelProperty(value = "活动名称")
	private String activityName;
	/** 活动介绍 */
    @ApiModelProperty(value = "活动介绍")
	private String activityContent;
	/** 开始时间 */
    @ApiModelProperty(value = "开始时间")
	private Date activityStartTime;
	/** 结束时间 */
    @ApiModelProperty(value = "结束时间")
	private Date activityEndTime;
	/** 举办地点 */
    @ApiModelProperty(value = "举办地点")
	private String activityAddress;
	/** 协会id */
    @ApiModelProperty(value = "协会id")
	private Integer activityAssociationId;
	/** 协会名称 */
    @ApiModelProperty(value = "协会名称")
	private String activityAssociationName;

}