package com.itSession.cn.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @ClassName: UserInfo
 * @description: 用户
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {
	/** 主键 */
    @ApiModelProperty(value = "主键")
	private Integer userId;
	/** 账号 */
    @ApiModelProperty(value = "账号")
	private String userAccount;
	/** 密码 */
    @ApiModelProperty(value = "密码")
	private String userPwd;
	/** 头像 */
    @ApiModelProperty(value = "头像")
	private String userImg;
	/** 姓名 */
    @ApiModelProperty(value = "姓名")
	private String userName;
	/** 协会id */
    @ApiModelProperty(value = "协会id")
	private Integer userAssociationId;
	/** 手机号 */
    @ApiModelProperty(value = "手机号")
	private String userPhone;
	/** 邮箱 */
    @ApiModelProperty(value = "邮箱")
	private String userEmail;
	/** 学校 */
    @ApiModelProperty(value = "学校")
	private String userSchool;
	/** 角色 1.管理员  2.普通用户 */
    @ApiModelProperty(value = "角色 0.super admin 1.管理员  2.普通用户")
	private String userRole;

}