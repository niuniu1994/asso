package com.itSession.cn.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: UserAssociationInfo
 * @description: 用户协会
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAssociationInfo {
	/** 主键 */
    @ApiModelProperty(value = "主键")
	private Integer userAssociationId;
	/** 账号 */
    @ApiModelProperty(value = "账号")
	private String userAssociationUserAccount;
	/** 名称 */
    @ApiModelProperty(value = "名称")
	private String userAssociationUserName;
	/** 协会id */
    @ApiModelProperty(value = "协会id")
	private Integer userAssociationAssociationId;
	/** 协会名称 */
    @ApiModelProperty(value = "协会名称")
	private String userAssociationAssociationName;
	/** 状态 1申请中  2.同意  3.拒绝 */
    @ApiModelProperty(value = "状态 1申请中  2.同意  3.拒绝")
	private Integer userAssociationStatus;

	
}