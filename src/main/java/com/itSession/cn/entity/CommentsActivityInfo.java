package com.itSession.cn.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ClassName: CommentsActivityInfo
 * @description: 评论活动
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentsActivityInfo {
	/** 主键标号 */
    @ApiModelProperty(value = "主键标号")
	private Integer commentsId;
	/** 评论用户 */
    @ApiModelProperty(value = "评论用户")
	private String commentsUserAccount;
	/** 用户姓名 */
    @ApiModelProperty(value = "用户姓名")
	private String commentsUserName;
	/** 评论内容 */
    @ApiModelProperty(value = "评论内容")
	private String commentsContent;
	/** 评论时间 */
    @ApiModelProperty(value = "评论时间")
	private Date commentsCreateDate;
	/** 活动id */
    @ApiModelProperty(value = "活动id")
	private Integer commentsActivityId;


	
}