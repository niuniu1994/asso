package com.itSession.cn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.itSession.cn.mapper")
@SpringBootApplication
public class SpellGroupApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpellGroupApplication.class, args);
    }
}
