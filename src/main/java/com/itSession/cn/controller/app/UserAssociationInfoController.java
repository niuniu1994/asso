package com.itSession.cn.controller.app;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.itSession.cn.util.Result;
import com.itSession.cn.util.ResultGenerator;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.util.StringUtils;

import com.itSession.cn.entity.UserAssociationInfo;
import com.itSession.cn.mapper.UserAssociationInfoMapper;

/**
 * @ClassName: UserAssociationInfoController
 * @description: UserAssociationInfoController
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Api(tags = "用户协会 模块")
@RequestMapping("/userAssociationInfo")
@RestController
public class UserAssociationInfoController {

    @Autowired
    private UserAssociationInfoMapper userAssociationInfoMapper;

    /**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
    @ApiOperation(value = "查询所有记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("list")
    public Result listAll() {
        try {
            List<UserAssociationInfo> list = userAssociationInfoMapper.listAll();
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 分页查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
    @ApiOperation(value = "查询所有记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("listByParam")
    public Result listByParam(String keyword) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("keyword", keyword);
            List<UserAssociationInfo> list = userAssociationInfoMapper.listByParam(map);
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 根据主键查询
     *
     * @param userAssociationId 主键
     * @return 返回记录，没有返回null
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "POST", notes = "加载数据", response = UserAssociationInfo.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userAssociationId", value = "主键", required = false, paramType = "query", dataType = "int")
    })
    @RequestMapping("getById")
    public Result getById(@ApiIgnore Integer userAssociationId) {
        try {
            UserAssociationInfo userAssociationInfo = userAssociationInfoMapper.getById(userAssociationId);
            return ResultGenerator.genSuccessResult(userAssociationInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }

    }


    /**
     * 新增，忽略null字段
     *
     * @param userAssociationInfo 新增的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "新增，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userAssociationId", value = "主键", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userAssociationUserAccount", value = "账号", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userAssociationUserName", value = "名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userAssociationAssociationId", value = "协会id", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userAssociationAssociationName", value = "协会名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userAssociationStatus", value = "状态 1申请中  2.同意  3.拒绝", required = false, paramType = "query", dataType = "int"),
    })
    @RequestMapping("insertIgnoreNull")
    public Result insertIgnoreNull(@ApiIgnore UserAssociationInfo userAssociationInfo) {
        try {
            userAssociationInfoMapper.insertIgnoreNull(userAssociationInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 修改，忽略null字段
     *
     * @param userAssociationInfo 修改的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "修改，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userAssociationId", value = "主键", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userAssociationUserAccount", value = "账号", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userAssociationUserName", value = "名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userAssociationAssociationId", value = "协会id", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userAssociationAssociationName", value = "协会名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userAssociationStatus", value = "状态 1申请中  2.同意  3.拒绝", required = false, paramType = "query", dataType = "int"),
    })
    @RequestMapping("updateIgnoreNull")
    public Result updateIgnoreNull(@ApiIgnore UserAssociationInfo userAssociationInfo) {
        try {
            userAssociationInfoMapper.updateIgnoreNull(userAssociationInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 删除记录
     *
     * @param userAssociationInfo 待删除的记录
     */
    @ApiOperation(value = "删除记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("delete")
    public Result delete(@ApiIgnore UserAssociationInfo userAssociationInfo) {
        try {
            userAssociationInfoMapper.delete(userAssociationInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


}