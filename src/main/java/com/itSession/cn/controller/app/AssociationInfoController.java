package com.itSession.cn.controller.app;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.itSession.cn.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.itSession.cn.util.Result;
import com.itSession.cn.util.ResultGenerator;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.util.StringUtils;

import com.itSession.cn.entity.AssociationInfo;
import com.itSession.cn.mapper.AssociationInfoMapper;

import javax.servlet.http.HttpSession;

/**
 * @ClassName: AssociationInfoController
 * @description: AssociationInfoController
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Api(tags = "协会模块")
@RequestMapping("/associationInfo")
@RestController
public class AssociationInfoController {

    @Autowired
    private AssociationInfoMapper associationInfoMapper;

    /* ----------------------------------------------------------------------------------------------------------------------------- */
    /**
     * 分页查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
    @ApiOperation(value = "查询所有记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("listByParam")
    public Result listByParam(HttpSession httpSession) {
        try {
            Map<String, Object> map = new HashMap<>();
            UserInfo userInfo = (UserInfo) httpSession.getAttribute("loginUserInfo");
            if (!userInfo.getUserRole().equals("1")) {
                map.put("keyword", userInfo.getUserAssociationId());
            }
            List<AssociationInfo> list = associationInfoMapper.listByParam(map);
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }
    /* ----------------------------------------------------------------------------------------------------------------------------- */

    /**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
    @ApiOperation(value = "查询所有记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("list")
    public Result listAll() {
        try {
            List<AssociationInfo> list = associationInfoMapper.listAll();
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

    /**
     * 根据主键查询
     *
     * @param associationId 主键
     * @return 返回记录，没有返回null
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "POST", notes = "加载数据", response = AssociationInfo.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "associationId", value = "主键编号", required = false, paramType = "query", dataType = "int")
    })
    @RequestMapping("getById")
    public Result getById(@ApiIgnore Integer associationId) {
        try {
            AssociationInfo associationInfo = associationInfoMapper.getById(associationId);
            return ResultGenerator.genSuccessResult(associationInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }

    }


    /**
     * 新增，忽略null字段
     *
     * @param associationInfo 新增的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "新增，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "associationId", value = "主键编号", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "associationName", value = "协会名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "associationContent", value = "协会介绍", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "associationImg", value = "协会照片", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "associationAddress", value = "协会地址", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "associationStatus", value = "状态：1.申请中  2.同意  3.拒绝  4.删除", required = false, paramType = "query", dataType = "string"),
    })
    @RequestMapping("insertIgnoreNull")
    public Result insertIgnoreNull(@ApiIgnore AssociationInfo associationInfo) {
        try {
            associationInfoMapper.insertIgnoreNull(associationInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 修改，忽略null字段
     *
     * @param associationInfo 修改的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "修改，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "associationId", value = "主键编号", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "associationName", value = "协会名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "associationContent", value = "协会介绍", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "associationImg", value = "协会照片", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "associationAddress", value = "协会地址", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "associationStatus", value = "状态：1.申请中  2.同意  3.拒绝  4.删除", required = false, paramType = "query", dataType = "string"),
    })
    @RequestMapping("updateIgnoreNull")
    public Result updateIgnoreNull(@ApiIgnore AssociationInfo associationInfo) {
        try {
            associationInfoMapper.updateIgnoreNull(associationInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 删除记录
     *
     * @param associationInfo 待删除的记录
     */
    @ApiOperation(value = "删除记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("delete")
    public Result delete(@ApiIgnore AssociationInfo associationInfo) {
        try {
            associationInfoMapper.delete(associationInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


}