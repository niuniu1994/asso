package com.itSession.cn.controller.app;

import com.itSession.cn.entity.*;
import com.itSession.cn.mapper.*;
import com.itSession.cn.util.Result;
import com.itSession.cn.util.ResultGenerator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: garbage-sys
 * @ClassName: AppController
 * @description:
 * @author: hanP
 * @create: 2021-03-12 19:49
 **/
@Api(tags = "APP流程模块")
@RequestMapping("/app")
@RestController
public class AppController {

    @Autowired
    private AssociationInfoMapper associationInfoMapper;
    @Autowired
    private ActivityInfoMapper activityInfoMapper;
    @Autowired
    private CommentsActivityInfoMapper commentsActivityInfoMapper;
    @Autowired
    private UserAssociationInfoMapper userAssociationInfoMapper;
    @Autowired
    private UserActivityInfoMapper userActivityInfoMapper;


    @ApiOperation(value = "查询所有协会", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("listAssociationInfo")
    public Result listAssociationInfo() {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("associationStatus", "2");
            List<AssociationInfo> list = associationInfoMapper.listByParam(map);
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

    @ApiOperation(value = "加入协会", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userAssociationUserAccount", value = "账号", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userAssociationUserName", value = "名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userAssociationAssociationId", value = "协会id", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userAssociationAssociationName", value = "协会名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userAssociationStatus", value = "状态 1申请中  2.同意  3.拒绝", required = false, paramType = "query", dataType = "int"),
    })
    @RequestMapping("insertUserAssociationInfo")
    public Result insertUserAssociationInfo(@ApiIgnore UserAssociationInfo userAssociationInfo) {
        try {
            userAssociationInfo.setUserAssociationStatus(1);
            userAssociationInfoMapper.insertIgnoreNull(userAssociationInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    @ApiOperation(value = "我加入的协会", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userAssociationUserAccount", value = "用户账号", required = false, paramType = "query", dataType = "String")
    })
    @RequestMapping("listByMyAssociation")
    public Result listByMyAssociation(String userAssociationUserAccount) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("userAssociationUserAccount", userAssociationUserAccount);
            List<UserAssociationInfo> list = userAssociationInfoMapper.listByParam(map);
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

    @ApiOperation(value = "退出协会", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userAssociationId", value = "用户协会id", required = false, paramType = "query", dataType = "int")
    })
    @RequestMapping("deleteUserAssociationInfo")
    public Result deleteUserAssociationInfo(@ApiIgnore UserAssociationInfo userAssociationInfo) {
        try {
            userAssociationInfoMapper.delete(userAssociationInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }






    @ApiOperation(value = "查询协会活动", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityAssociationId", value = "协会主键编号", required = false, paramType = "query", dataType = "int")
    })
    @RequestMapping("listActivityInfo")
    public Result listActivityInfo(Integer activityAssociationId) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("activityAssociationId", activityAssociationId);
            List<ActivityInfo> list = activityInfoMapper.listByAssociationId(map);
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

    @ApiOperation(value = "查询活动评论", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentsActivityId", value = "协会主键编号", required = false, paramType = "query", dataType = "int")
    })
    @RequestMapping("listByCommentsActivityInfo")
    public Result listByCommentsActivityInfo(Integer commentsActivityId) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("commentsActivityId", commentsActivityId);
            List<CommentsActivityInfo> list = commentsActivityInfoMapper.listByParam(map);
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

    @ApiOperation(value = "添加活动评论", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentsUserAccount", value = "评论用户", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsUserName", value = "用户姓名", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsContent", value = "评论内容", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsCreateDate", value = "评论时间", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsActivityId", value = "活动id", required = false, paramType = "query", dataType = "int"),
    })
    @RequestMapping("insertCommentsActivity")
    public Result insertCommentsActivity(@ApiIgnore CommentsActivityInfo commentsActivityInfo) {
        try {
            commentsActivityInfoMapper.insertIgnoreNull(commentsActivityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

    /*@ApiOperation(value = "注册活动", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityName", value = "活动名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityContent", value = "活动介绍", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityStartTime", value = "开始时间", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityEndTime", value = "结束时间", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityAddress", value = "举办地点", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityAssociationId", value = "协会id", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "activityAssociationName", value = "协会名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activitySchool", value = "学校名称", required = false, paramType = "query", dataType = "string"),
    })
    @RequestMapping("zhuCeActivityInfo")
    public Result zhuCeActivityInfo(@ApiIgnore ActivityInfo activityInfo) {
        try {
            activityInfoMapper.insertIgnoreNull(activityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }*/

    @ApiOperation(value = "参加活动", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userActivityUserAccount", value = "账号", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userActivityUserName", value = "姓名", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userActivityActivityId", value = "活动id", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userActivityActivityName", value = "活动名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userActivityStstue", value = "状态 1申请中  2.同意  3.拒绝", required = false, paramType = "query", dataType = "string"),
    })
    @RequestMapping("insertUserActivity")
    public Result insertUserActivity(@ApiIgnore UserActivityInfo userActivityInfo) {
        try {
            userActivityInfo.setUserActivityStstue("1");
            userActivityInfoMapper.insertIgnoreNull(userActivityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

    @ApiOperation(value = "我参加的活动", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userActivityUserAccount", value = "账号", required = false, paramType = "query", dataType = "string"),
    })
    @RequestMapping("listUserActivityInfo")
    public Result listUserActivityInfo(String userActivityUserAccount) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("userActivityUserAccount", userActivityUserAccount);
            List<UserActivityInfo> list = userActivityInfoMapper.listByParam(map);
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

    @ApiOperation(value = "取消参加活动", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userActivityId", value = "主键", required = false, paramType = "query", dataType = "int"),
    })
    @RequestMapping("deleteUserActivityInfo")
    public Result delete(@ApiIgnore UserActivityInfo userActivityInfo) {
        try {
            userActivityInfoMapper.delete(userActivityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

}
