package com.itSession.cn.controller.app;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.itSession.cn.util.Result;
import com.itSession.cn.util.ResultGenerator;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.util.StringUtils;

import com.itSession.cn.entity.UserActivityInfo;
import com.itSession.cn.mapper.UserActivityInfoMapper;

/**
 * @ClassName: UserActivityInfoController
 * @description: UserActivityInfoController
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Api(tags = "用户活动 模块")
@RequestMapping("/userActivityInfo")
@RestController
public class UserActivityInfoController {

    @Autowired
    private UserActivityInfoMapper userActivityInfoMapper;

    /**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
    @ApiOperation(value = "查询所有记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("list")
    public Result listAll() {
        try {
            List<UserActivityInfo> list = userActivityInfoMapper.listAll();
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 分页查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
    @ApiOperation(value = "查询所有记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("listByParam")
    public Result listByParam(String keyword) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("keyword", keyword);
            List<UserActivityInfo> list = userActivityInfoMapper.listByParam(map);
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 根据主键查询
     *
     * @param userActivityId 主键
     * @return 返回记录，没有返回null
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "POST", notes = "加载数据", response = UserActivityInfo.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userActivityId", value = "主键", required = false, paramType = "query", dataType = "int")
    })
    @RequestMapping("getById")
    public Result getById(@ApiIgnore Integer userActivityId) {
        try {
            UserActivityInfo userActivityInfo = userActivityInfoMapper.getById(userActivityId);
            return ResultGenerator.genSuccessResult(userActivityInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }

    }


    /**
     * 新增，忽略null字段
     *
     * @param userActivityInfo 新增的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "新增，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userActivityId", value = "主键", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userActivityUserAccount", value = "账号", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userActivityUserName", value = "姓名", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userActivityActivityId", value = "活动id", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userActivityActivityName", value = "活动名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userActivityStstue", value = "状态 1申请中  2.同意  3.拒绝", required = false, paramType = "query", dataType = "string"),
    })
    @RequestMapping("insertIgnoreNull")
    public Result insertIgnoreNull(@ApiIgnore UserActivityInfo userActivityInfo) {
        try {
            userActivityInfoMapper.insertIgnoreNull(userActivityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 修改，忽略null字段
     *
     * @param userActivityInfo 修改的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "修改，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userActivityId", value = "主键", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userActivityUserAccount", value = "账号", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userActivityUserName", value = "姓名", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userActivityActivityId", value = "活动id", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userActivityActivityName", value = "活动名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userActivityStstue", value = "状态 1申请中  2.同意  3.拒绝", required = false, paramType = "query", dataType = "string"),
    })
    @RequestMapping("updateIgnoreNull")
    public Result updateIgnoreNull(@ApiIgnore UserActivityInfo userActivityInfo) {
        try {
            userActivityInfoMapper.updateIgnoreNull(userActivityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 删除记录
     *
     * @param userActivityInfo 待删除的记录
     */
    @ApiOperation(value = "删除记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("delete")
    public Result delete(@ApiIgnore UserActivityInfo userActivityInfo) {
        try {
            userActivityInfoMapper.delete(userActivityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


}