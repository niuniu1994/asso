package com.itSession.cn.controller.app;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.itSession.cn.util.Result;
import com.itSession.cn.util.ResultGenerator;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.util.StringUtils;

import com.itSession.cn.entity.CommentsActivityInfo;
import com.itSession.cn.mapper.CommentsActivityInfoMapper;

import javax.servlet.http.HttpSession;

/**
 * @ClassName: CommentsActivityInfoController
 * @description: CommentsActivityInfoController
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Api(tags = "评论活动 模块")
@RequestMapping("/commentsActivityInfo")
@RestController
public class CommentsActivityInfoController {

    @Autowired
    private CommentsActivityInfoMapper commentsActivityInfoMapper;

/*------------------------------------------------------------------------------------------------------------*/

    @ApiOperation(value = "根据活动id查询评论", httpMethod = "GET", notes = "加载数据", response = CommentsActivityInfo.class)
    @GetMapping("activity/{activityId}")
    public Result getCommentsOfActivity(@PathVariable("activityId") Integer activityId){
        try {
            List<CommentsActivityInfo> list = commentsActivityInfoMapper.getCommentsByActivityId(activityId);
            return ResultGenerator.genSuccessResult(list);
        }catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

    @ApiOperation(value = "根据协会id查询评论", httpMethod = "GET", notes = "加载数据", response = CommentsActivityInfo.class)
    @GetMapping("association")
    public Result getCommentsOfAssociation(HttpSession session){
        List<CommentsActivityInfo> list = new ArrayList<>();

        try {
            String role = (String) session.getAttribute("loginRole");
            //如果是超级管理员则返回全部
            if ("0".equals(role)){
                list = commentsActivityInfoMapper.listAll();
            }else {
                Integer activityId =(Integer) session.getAttribute("loginAssociationId");
                list = commentsActivityInfoMapper.getCommentsByActivityId(activityId);
            }
            return ResultGenerator.genSuccessResult(list);
        }catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }

/*------------------------------------------------------------------------------------------------------------*/
    /**
     * 根据主键查询
     *
     * @param commentsId 主键
     * @return 返回记录，没有返回null
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "POST", notes = "加载数据", response = CommentsActivityInfo.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentsId", value = "主键标号", required = false, paramType = "query", dataType = "int")
    })
    @RequestMapping("getById")
    public Result getById(@ApiIgnore Integer commentsId) {
        try {
            CommentsActivityInfo commentsActivityInfo = commentsActivityInfoMapper.getById(commentsId);
            return ResultGenerator.genSuccessResult(commentsActivityInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }

    }


    /**
     * 新增，忽略null字段
     *
     * @param commentsActivityInfo 新增的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "新增，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentsId", value = "主键标号", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "commentsUserAccount", value = "评论用户", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsUserName", value = "用户姓名", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsContent", value = "评论内容", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsCreateDate", value = "评论时间", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsActivityId", value = "活动id", required = false, paramType = "query", dataType = "int"),
    })
    @RequestMapping("insertIgnoreNull")
    public Result insertIgnoreNull(@ApiIgnore CommentsActivityInfo commentsActivityInfo) {
        try {
            commentsActivityInfoMapper.insertIgnoreNull(commentsActivityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 修改，忽略null字段
     *
     * @param commentsActivityInfo 修改的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "修改，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentsId", value = "主键标号", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "commentsUserAccount", value = "评论用户", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsUserName", value = "用户姓名", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsContent", value = "评论内容", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsCreateDate", value = "评论时间", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "commentsActivityId", value = "活动id", required = false, paramType = "query", dataType = "int"),
    })
    @RequestMapping("updateIgnoreNull")
    public Result updateIgnoreNull(@ApiIgnore CommentsActivityInfo commentsActivityInfo) {
        try {
            commentsActivityInfoMapper.updateIgnoreNull(commentsActivityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 删除记录
     *
     * @param commentsActivityInfo 待删除的记录
     */
    @ApiOperation(value = "删除记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("delete")
    public Result delete(@ApiIgnore CommentsActivityInfo commentsActivityInfo) {
        try {
            commentsActivityInfoMapper.delete(commentsActivityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


}