package com.itSession.cn.controller.app;

import com.itSession.cn.entity.UserInfo;
import com.itSession.cn.exception.ResponseData;
import com.itSession.cn.mapper.UserInfoMapper;
import com.itSession.cn.util.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;

/**
 * @program: demo
 * @ClassName: DemoLoginController
 * @description: 登录模块
 * @author: hanP
 * @create: 2020-12-24 11:01
 **/
@Api(tags = "APP登录模块")
@RequestMapping("/app/comments")
@RestController
public class DemoLoginController {


    @Autowired
    private UserInfoMapper userInfoMapper;


    @ApiOperation(value = "普通用户登陆", httpMethod = "Get", notes = "加载数据", response = ResponseData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "demoUserAccount", value = "账号", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "demoUserPwd", value = "密码", required = false, paramType = "query", dataType = "string")
    })
    @GetMapping("login")
    public ResponseData loginIn(@ApiIgnore String demoUserAccount, @ApiIgnore String demoUserPwd) {
        try {
            UserInfo user = userInfoMapper.getUserByAccount(demoUserAccount);
            if (demoUserPwd.equals(user.getUserPwd())) {
                return new ResponseData().data(user).success();
            }
            return new ResponseData().fail(10003, "用户名或密码错误");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseData().fail(10002, "用户不存在");
        }
    }



    /**
     * 注册
     *
     * @param demoUser 注册的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "注册", httpMethod = "POST", notes = "加载数据", response = UserInfo.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userAccount", value = "手机号", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userName", value = "姓名", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userPwd", value = "密码", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "userGender", value = "性别", required = false, paramType = "query", dataType = "string")
    })
    @RequestMapping("registered")
    public ResponseData registered(@ApiIgnore UserInfo demoUser) {
        try {
            UserInfo user = userInfoMapper.getUserByAccount(demoUser.getUserAccount());
            if (null == user) {
                userInfoMapper.insertIgnoreNull(demoUser);
            } else {
                return new ResponseData().fail(10001, "账号已存在，请更换。");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseData().fail(500, e.getMessage());
        }
        return new ResponseData().success();
    }


}
