package com.itSession.cn.controller.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.itSession.cn.util.Result;
import com.itSession.cn.util.ResultGenerator;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.util.StringUtils;

import com.itSession.cn.entity.UserInfo;
import com.itSession.cn.mapper.UserInfoMapper;

import javax.servlet.http.HttpSession;

/**
 * @ClassName: UserInfoController
 * @description: UserInfoController
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Api(tags = "用户 模块")
@RequestMapping("/userInfo")
@RestController
public class UserInfoController {

    @Autowired
    private UserInfoMapper userInfoMapper;


    /* ----------------------------------------------------------------------------------------------------------------------------- */

    /**
     * get users by association id
     *
     * @return 返回集合，没有返回空List
     */
    @ApiOperation(value = "查询所有注册会员", httpMethod = "Get", notes = "加载数据", response = Result.class)
    @GetMapping("association")
    public Result getUsersOfAssociation(HttpSession session) {

        List<UserInfo> list = new ArrayList<>();
        // 如果是超级管理员 返回全部否则就返回 协会所属的
        try {
            String role = (String) session.getAttribute("loginRole");
            if ("1".equals(role)){
                list = userInfoMapper.getUsersByAssociationId((Integer) session.getAttribute("loginAssociationId"));
            }else {
                list = userInfoMapper.listAll();
            }
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500,e.getMessage());
        }
    }

    /**
     * get users by activityId
     *
     * @return 返回集合，没有返回空List
     */
    @ApiOperation(value = "查询所有注册会员", httpMethod = "Get", notes = "加载数据", response = Result.class)
    @GetMapping("/activity/{activityId}")
    public Result getUsersOfActivity(@PathVariable("activityId") Integer activityId) {

        List<UserInfo> list = new ArrayList<>();
        try {
            list = userInfoMapper.getUsersByActivityId(activityId);
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500,e.getMessage());
        }
    }



    /* -------------------------------------------------------------------------------------- */

    





    /**
     * 根据主键查询
     *
     * @param userId 主键
     * @return 返回记录，没有返回null
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "POST", notes = "加载数据", response = UserInfo.class)
     @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "主键", required = false, paramType = "query" ,dataType="int")
    })
    @RequestMapping("getById")
    public Result getById(@ApiIgnore Integer userId) {
    	try {
    	 	UserInfo userInfo = userInfoMapper.getById(userId);
         	return ResultGenerator.genSuccessResult(userInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
       
    }
    
    
    /**
     * 新增，忽略null字段
     *
     * @param userInfo 新增的记录
     * @return 返回影响行数
     */
      @ApiOperation(value = "新增，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
                @ApiImplicitParam(name = "userId", value = "主键", required = false, paramType = "query" ,dataType="int"),
                @ApiImplicitParam(name = "userAccount", value = "账号", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userPwd", value = "密码", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userImg", value = "头像", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userName", value = "姓名", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userAssociationId", value = "协会id", required = false, paramType = "query" ,dataType="int"),
                @ApiImplicitParam(name = "userPhone", value = "手机号", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userEmail", value = "邮箱", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userSchool", value = "学校", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userRole", value = "角色 1.管理员  2.普通用户", required = false, paramType = "query" ,dataType="string"),
        })
    @RequestMapping("insertIgnoreNull")
    public Result insertIgnoreNull(@ApiIgnore UserInfo userInfo) {
    	try {
    		userInfoMapper.insertIgnoreNull(userInfo);
            return ResultGenerator.genSuccessResult();
       } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500,e.getMessage());
        }
    }
    
    
    /**
     * 修改，忽略null字段
     *
     * @param userInfo 修改的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "修改，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
                @ApiImplicitParam(name = "userId", value = "主键", required = false, paramType = "query" ,dataType="int"),
                @ApiImplicitParam(name = "userAccount", value = "账号", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userPwd", value = "密码", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userImg", value = "头像", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userName", value = "姓名", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userAssociationId", value = "协会id", required = false, paramType = "query" ,dataType="int"),
                @ApiImplicitParam(name = "userPhone", value = "手机号", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userEmail", value = "邮箱", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userSchool", value = "学校", required = false, paramType = "query" ,dataType="string"),
                @ApiImplicitParam(name = "userRole", value = "角色 1.管理员  2.普通用户", required = false, paramType = "query" ,dataType="string"),
        })
    @RequestMapping("updateIgnoreNull")
    public Result updateIgnoreNull(@ApiIgnore UserInfo userInfo) {
    	 try {
         	 userInfoMapper.updateIgnoreNull(userInfo);
            return ResultGenerator.genSuccessResult();
         } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500,e.getMessage());
        }
    }
    
    
     /**
     * 删除记录
     *
     * @param userInfo 待删除的记录
     */
    @ApiOperation(value = "删除记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("delete")
    public Result delete(@ApiIgnore UserInfo userInfo) {
    	 try {
         	 userInfoMapper.delete(userInfo);
             return ResultGenerator.genSuccessResult();
         } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500,e.getMessage());
        }
    }
    
   
    
}