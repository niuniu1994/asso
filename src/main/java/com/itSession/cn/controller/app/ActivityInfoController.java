package com.itSession.cn.controller.app;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.itSession.cn.util.Result;
import com.itSession.cn.util.ResultGenerator;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.util.StringUtils;

import com.itSession.cn.entity.ActivityInfo;
import com.itSession.cn.mapper.ActivityInfoMapper;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

/**
 * @ClassName: ActivityInfoController
 * @description: ActivityInfoController
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Api(tags = "活动模块")
@RequestMapping("/activityInfo")
@RestController
public class ActivityInfoController {

    @Autowired
    private ActivityInfoMapper activityInfoMapper;


    /* ----------------------------------------------------------------------------------------------------------------------------- */

    /**
     *
     * 根据协会id查询活动
     *
     * @return 返回集合，没有返回空List
     */
    @ApiOperation(value = "根据协会id查询活动", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("listAssociations")
    public Result listByParam(HttpSession session) {
        try {
            Map<String, Object> map = new HashMap<>();
            String role = (String) session.getAttribute("loginRole");

            // 超级管理员 返回全部 否则根据 管理员所属协会返回
            if ("1".equals(role)) {
                map.put("activityAssociationId", session.getAttribute("loginAssociationId"));
            } else {
                map.put("activityAssociationId", null);
            }

            List<ActivityInfo> list = activityInfoMapper.listByAssociationId(map);
            return ResultGenerator.genSuccessResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }
    /* ----------------------------------------------------------------------------------------------------------------------------- */


    /**
     * 根据主键查询
     *
     * @param activityId 主键
     * @return 返回记录，没有返回null
     */
    @ApiOperation(value = "根据主键查询", httpMethod = "POST", notes = "加载数据", response = ActivityInfo.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "主键编号", required = false, paramType = "query", dataType = "int")
    })
    @RequestMapping("getById")
    public Result getById(@ApiIgnore Integer activityId) {
        try {
            ActivityInfo activityInfo = activityInfoMapper.getById(activityId);
            return ResultGenerator.genSuccessResult(activityInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }

    }


    /**
     * 新增，忽略null字段
     *
     * @param activityInfo 新增的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "新增，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "主键编号", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "activityName", value = "活动名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityContent", value = "活动介绍", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityStartTime", value = "开始时间", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityEndTime", value = "结束时间", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityAddress", value = "举办地点", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityAssociationId", value = "协会id", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "activityAssociationName", value = "协会名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activitySchool", value = "学校名称", required = false, paramType = "query", dataType = "string"),
    })
    @RequestMapping("insertIgnoreNull")
    public Result insertIgnoreNull(@ApiIgnore ActivityInfo activityInfo) {
        try {
            activityInfoMapper.insertIgnoreNull(activityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 修改，忽略null字段
     *
     * @param activityInfo 修改的记录
     * @return 返回影响行数
     */
    @ApiOperation(value = "修改，忽略null字段", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "activityId", value = "主键编号", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "activityName", value = "活动名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityContent", value = "活动介绍", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityStartTime", value = "开始时间", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityEndTime", value = "结束时间", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityAddress", value = "举办地点", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activityAssociationId", value = "协会id", required = false, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "activityAssociationName", value = "协会名称", required = false, paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "activitySchool", value = "学校名称", required = false, paramType = "query", dataType = "string"),
    })
    @RequestMapping("updateIgnoreNull")
    public Result updateIgnoreNull(@ApiIgnore ActivityInfo activityInfo) {
        try {
            activityInfoMapper.updateIgnoreNull(activityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


    /**
     * 删除记录
     *
     * @param activityInfo 待删除的记录
     */
    @ApiOperation(value = "删除记录", httpMethod = "POST", notes = "加载数据", response = Result.class)
    @RequestMapping("delete")
    public Result delete(@ApiIgnore ActivityInfo activityInfo) {
        try {
            activityInfoMapper.delete(activityInfo);
            return ResultGenerator.genSuccessResult();
        } catch (Exception e) {
            e.printStackTrace();
            return ResultGenerator.genErrorResult(500, e.getMessage());
        }
    }


}