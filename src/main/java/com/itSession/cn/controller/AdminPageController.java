package com.itSession.cn.controller;

import com.itSession.cn.entity.*;
import com.itSession.cn.mapper.*;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @program: travel_consulting
 * @ClassName: AdminPageController
 * @description:
 * @author: hanP
 * @create: 2021-02-01 10:57
 **/
@Api(tags = "后台管理模块")
@Controller
@RequestMapping("/admin")
public class AdminPageController {


    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private ActivityInfoMapper activityInfoMapper;
    @Resource
    private AssociationInfoMapper associationInfoMapper;
    @Autowired
    private CommentsActivityInfoMapper commentsActivityInfoMapper;

    @Autowired
    private UserActivityInfoMapper userActivityInfoMapper;
    @Autowired
    private UserAssociationInfoMapper userAssociationInfoMapper;


    /*****userAssociationInfo*****/
    @GetMapping("/userAssociationInfo")
    public String userAssociationInfoPage(HttpServletRequest request) {
        request.setAttribute("path", "userAssociationInfo");
        return "admin/userAssociationInfo-list";

    }

    @GetMapping("/userAssociationInfo/edit")
    public String userAssociationInfoEdit(HttpServletRequest request) {
        request.setAttribute("path", "userAssociationInfo");
        return "admin/userAssociationInfo-edit";
    }

    @GetMapping("/userAssociationInfo/edit/{userId}")
    public String userAssociationInfoEdit(HttpServletRequest request, @PathVariable("userId") int userId) {
        UserAssociationInfo byId = userAssociationInfoMapper.getById(userId);
        request.setAttribute("goods", byId);
        request.setAttribute("path", "userAssociationInfo");
        return "admin/userAssociationInfo-edit";
    }


    /*****userActivityInfo*****/


    @GetMapping("/userActivityInfo")
    public String userActivityInfoPage(HttpServletRequest request) {
        request.setAttribute("path", "userActivityInfo");
        return "admin/userActivityInfo-list";
    }

    @GetMapping("/userActivityInfo/edit")
    public String userActivityInfoEdit(HttpServletRequest request) {
        request.setAttribute("path", "userActivityInfo");
        return "admin/userActivityInfo-edit";
    }

    @GetMapping("/userActivityInfo/edit/{userId}")
    public String userActivityInfoEdit(HttpServletRequest request, @PathVariable("userId") int userId) {
        UserActivityInfo byId = userActivityInfoMapper.getById(userId);
        request.setAttribute("goods", byId);
        request.setAttribute("path", "userActivityInfo");
        return "admin/userActivityInfo-edit";
    }



    /*****commentsActivityInfo*****/
    @GetMapping("/commentsActivityInfo")
    public String commentsActivityInfoPage(HttpServletRequest request) {
        request.setAttribute("path", "commentsActivityInfo");
        return "admin/commentsActivityInfo-list";
    }

    @GetMapping("/commentsActivityInfo/{activityId}")
    public String commentsActivityInfoPageById(HttpServletRequest request,@PathVariable("activityId") Integer activityId) {
        request.setAttribute("path", "commentsActivityInfo");
        return "admin/commentsActivityInfo-list";
    }

    @GetMapping("/commentsActivityInfo/edit")
    public String commentsActivityInfoEdit(HttpServletRequest request) {
        request.setAttribute("path", "commentsActivityInfo");
        return "admin/commentsActivityInfo-edit";
    }

    @GetMapping("/commentsActivityInfo/edit/{userId}")
    public String commentsActivityInfoEdit(HttpServletRequest request, @PathVariable("userId") int userId) {
        CommentsActivityInfo byId = commentsActivityInfoMapper.getById(userId);
        request.setAttribute("goods", byId);
        request.setAttribute("path", "commentsActivityInfo");
        return "admin/commentsActivityInfo-edit";
    }


    /*****associationInfo*****/
    @GetMapping("/associationInfo")
    public String associationInfoPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException {

        String role = (String) session.getAttribute("loginRole");
        //如果是超级管理员 显示协会列表
        if ("0".equals(role)){
            request.setAttribute("path", "associationInfo");
            return "admin/associationInfo-list";
        }
        //否则跳到修改界面
        response.sendRedirect("/admin/associationInfo/edit/" + session.getAttribute("loginAssociationId"));
        return null;
    }

    @GetMapping("/associationInfo/edit")
    public String associationInfoEdit(HttpServletRequest request) {
        request.setAttribute("path", "associationInfo");
        return "admin/associationInfo-edit";
    }

    @GetMapping("/associationInfo/edit/{associationId}")
    public String associationInfoEdit(HttpServletRequest request, @PathVariable("associationId") int associationId) {
        AssociationInfo byId = associationInfoMapper.getById(associationId);
        request.setAttribute("goods", byId);
        request.setAttribute("path", "associationInfo");
        return "admin/associationInfo-edit";
    }


    /*****activityInfo*****/
    @GetMapping("/activityInfo")
    public String activityInfoPage(HttpServletRequest request) {
        request.setAttribute("path", "activityInfo");
        return "admin/activityInfo-list";
    }

    @GetMapping("/activityInfo/edit")
    public String activityInfoEdit(HttpServletRequest request) {
        request.setAttribute("path", "activityInfo");
        return "admin/activityInfo-edit";
    }

    @GetMapping("/activityInfo/edit/{userId}")
    public String activityInfoEdit(HttpServletRequest request, @PathVariable("userId") int userId) {
        ActivityInfo byId = activityInfoMapper.getById(userId);
        request.setAttribute("goods", byId);
        request.setAttribute("path", "activityInfo");
        return "admin/activityInfo-edit";
    }


    /*****userInfo*****/
    @GetMapping("/userInfo/{activityId}")
    public String userOfActivity(@PathVariable("activityId") Integer activityId,HttpServletRequest request) {
        request.setAttribute("path", "userInfo");
        return "admin/userInfo-list";
    }

    @GetMapping("/userInfo")
    public String usersPage(HttpServletRequest request) {
        request.setAttribute("path", "userInfo");
        return "admin/userInfo-list";
    }

    @GetMapping("/userInfo/edit")
    public String editUser(HttpServletRequest request) {
        request.setAttribute("path", "userInfo");
        return "admin/userInfo-edit";
    }

    @GetMapping("/userInfo/edit/{userId}")
    public String editUser(HttpServletRequest request, @PathVariable("userId") int userId) {
        UserInfo byId = userInfoMapper.getById(userId);
        request.setAttribute("goods", byId);
        request.setAttribute("path", "userInfo");
        return "admin/userInfo-edit";
    }


    @GetMapping("/profile")
    public String profile(HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("loginUserId");
        UserInfo userByAccount = userInfoMapper.getUserByAccount(userName);
        if (userByAccount == null) {
            return "admin/login";
        }
        request.setAttribute("path", "profile");
        request.setAttribute("adminUserInfo", userByAccount);
        return "admin/profile";
    }


    @GetMapping({"/login"})
    public String login() {
        return "admin/login";
    }

    @GetMapping({"/test"})
    public String test() {
        return "admin/test";
    }

    @GetMapping({"", "/", "/index", "/index.html"})
    public String index(HttpServletRequest request) {
        request.setAttribute("path", "index");
        return "admin/index";
    }

    @PostMapping(value = "/login")
    public String login(@RequestParam("userName") String userName,
                        @RequestParam("password") String password,
                        HttpSession session) {
        if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)) {
            session.setAttribute("errorMsg", "Username and password can't be empty");
            return "admin/login";
        }
        UserInfo user = userInfoMapper.getUserByAccount(userName);
        if (user != null) {
            if (!password.equals(user.getUserPwd())) {
                session.setAttribute("errorMsg", "Username or password is not correct");
                return "admin/login";
            }

            if( "1".equals(user.getUserRole()) || "0".equals(user.getUserRole())){
                session.setAttribute("loginUser", user.getUserName());
                session.setAttribute("loginRole",user.getUserRole());
                session.setAttribute("loginUserId", user.getUserAccount());
                session.setAttribute("loginUserId", user.getUserRole());
                session.setAttribute("loginAssociationId",user.getUserAssociationId());
                session.setAttribute("loginUserInfo", user);
                //session过期时间设置为7200秒 即两小时
                session.setMaxInactiveInterval(60 * 60 * 2);
                return "redirect:/admin/index";
            }else {
                session.setAttribute("errorMsg", "User access right limited");
                return "admin/login";
            }

        } else {
            session.setAttribute("errorMsg", "Login failed");
            return "admin/login";
        }
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request) {
        request.getSession().removeAttribute("loginUserId");
        request.getSession().removeAttribute("loginUser");
        request.getSession().removeAttribute("errorMsg");
        return "admin/login";
    }


}
