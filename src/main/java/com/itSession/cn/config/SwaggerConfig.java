package com.itSession.cn.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * 创建一个Docket的对象，相当于是swagger的一个实例
     */
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.OAS_30)
                //只有当springboot配置文件为开发环境时，才开启swaggerAPI文档功能
                .enable(true)
                .select()
                //RequestHandlerSelectors,配置要扫描接口的方式
                //basePackage：指定扫描的包路径
                //any：扫描全部
                //none：全部不扫描
                //withClassAnnotation:扫描类上的注解，如RestController
                //withMethodAnnotation:扫描方法上的注解，如GetMapping
                .apis(RequestHandlerSelectors.basePackage("com.itSession.cn.controller"))
                //设置对应的路径才获取
                //.paths(PathSelectors.ant("/hello"))
                .build()
                .apiInfo(apiInfo());

    }

    /**
     * 配置相关的api信息
     */
    private ApiInfo apiInfo() {
        Contact contact = new Contact("spell-group", "https://www.spell-group.com/", "spell-group@spell-group.com");
        return new ApiInfo(
                "spell-group的swaggerAPI文档",
                "spell-group",
                "1.0",
                "urn:tos",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<>());
    }
}
