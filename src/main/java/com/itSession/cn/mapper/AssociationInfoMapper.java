package com.itSession.cn.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import com.itSession.cn.entity.AssociationInfo;

import javax.naming.Name;

/**
 * @ClassName: AssociationInfo
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Mapper
public interface AssociationInfoMapper {

	/**
	 * 查询所有记录
	 *
	 * @return 返回集合，没有返回空List
	 */
	List<AssociationInfo> listAll();


	/**
	 * 分页查询所有记录
	 *
	 * @param map 参数map集合
	 * @return 返回集合，没有返回空List
	 */
	List<AssociationInfo> listByParam(Map<String, Object> map);


	/**
	 * 根据主键查询
	 *
	 * @param associationId 主键
	 * @return 返回记录，没有返回null
	 */
	AssociationInfo getById(Integer associationId);

	/**
	 * 新增，插入所有字段
	 *
	 * @param associationInfo 新增的记录
	 * @return 返回影响行数
	 */
	int insert(AssociationInfo associationInfo);

	/**
	 * 新增，忽略null字段
	 *
	 * @param associationInfo 新增的记录
	 * @return 返回影响行数
	 */
	int insertIgnoreNull(AssociationInfo associationInfo);

	/**
	 * 修改，修改所有字段
	 *
	 * @param associationInfo 修改的记录
	 * @return 返回影响行数
	 */
	int update(AssociationInfo associationInfo);

	/**
	 * 修改，忽略null字段
	 *
	 * @param associationInfo 修改的记录
	 * @return 返回影响行数
	 */
	int updateIgnoreNull(AssociationInfo associationInfo);

	/**
	 * 删除记录
	 *
	 * @param associationInfo 待删除的记录
	 * @return 返回影响行数
	 */
	int delete(AssociationInfo associationInfo);

}