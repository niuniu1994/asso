package com.itSession.cn.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import com.itSession.cn.entity.UserActivityInfo;

/**
 * @ClassName: UserActivityInfo
 * @description: 用户活动
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Mapper
public interface UserActivityInfoMapper {

	/**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
	List<UserActivityInfo> listAll();
    
    
    /**
     * 分页查询所有记录
     *
     * @param map 参数map集合
     * @return 返回集合，没有返回空List
     */
    List<UserActivityInfo> listByParam(Map<String, Object> map);


	/**
     * 根据主键查询
     *
     * @param userActivityId 主键
     * @return 返回记录，没有返回null
     */
	UserActivityInfo getById(Integer userActivityId);
	
	/**
     * 新增，插入所有字段
     *
     * @param userActivityInfo 新增的记录
     * @return 返回影响行数
     */
	int insert(UserActivityInfo userActivityInfo);
	
	/**
     * 新增，忽略null字段
     *
     * @param userActivityInfo 新增的记录
     * @return 返回影响行数
     */
	int insertIgnoreNull(UserActivityInfo userActivityInfo);
	
	/**
     * 修改，修改所有字段
     *
     * @param userActivityInfo 修改的记录
     * @return 返回影响行数
     */
	int update(UserActivityInfo userActivityInfo);
	
	/**
     * 修改，忽略null字段
     *
     * @param userActivityInfo 修改的记录
     * @return 返回影响行数
     */
	int updateIgnoreNull(UserActivityInfo userActivityInfo);
	
	/**
     * 删除记录
     *
     * @param userActivityInfo 待删除的记录
     * @return 返回影响行数
     */
	int delete(UserActivityInfo userActivityInfo);
	
}