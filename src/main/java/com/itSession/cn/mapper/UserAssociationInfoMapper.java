package com.itSession.cn.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import com.itSession.cn.entity.UserAssociationInfo;

/**
 * @ClassName: UserAssociationInfo
 * @description: 用户协会
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */

@Mapper
public interface UserAssociationInfoMapper {

	/**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
	List<UserAssociationInfo> listAll();
    
    
    /**
     * 分页查询所有记录
     *
     * @param map 参数map集合
     * @return 返回集合，没有返回空List
     */
    List<UserAssociationInfo> listByParam(Map<String, Object> map);


	/**
     * 根据主键查询
     *
     * @param userAssociationId 主键
     * @return 返回记录，没有返回null
     */
	UserAssociationInfo getById(Integer userAssociationId);
	
	/**
     * 新增，插入所有字段
     *
     * @param userAssociationInfo 新增的记录
     * @return 返回影响行数
     */
	int insert(UserAssociationInfo userAssociationInfo);
	
	/**
     * 新增，忽略null字段
     *
     * @param userAssociationInfo 新增的记录
     * @return 返回影响行数
     */
	int insertIgnoreNull(UserAssociationInfo userAssociationInfo);
	
	/**
     * 修改，修改所有字段
     *
     * @param userAssociationInfo 修改的记录
     * @return 返回影响行数
     */
	int update(UserAssociationInfo userAssociationInfo);
	
	/**
     * 修改，忽略null字段
     *
     * @param userAssociationInfo 修改的记录
     * @return 返回影响行数
     */
	int updateIgnoreNull(UserAssociationInfo userAssociationInfo);
	
	/**
     * 删除记录
     *
     * @param userAssociationInfo 待删除的记录
     * @return 返回影响行数
     */
	int delete(UserAssociationInfo userAssociationInfo);
	
}