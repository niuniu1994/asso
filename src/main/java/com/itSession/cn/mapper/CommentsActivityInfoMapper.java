package com.itSession.cn.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import com.itSession.cn.entity.CommentsActivityInfo;

/**
 * @ClassName: CommentsActivityInfo
 * @description: 评论活动
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Mapper
public interface CommentsActivityInfoMapper {


	/**
	 * get comments by activityId
	 */

	List<CommentsActivityInfo> getCommentsByActivityId(Integer id);


	List<CommentsActivityInfo> getCommentByAssociationId(Integer id);
	/**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
	List<CommentsActivityInfo> listAll();
    
    
    /**
     * 分页查询所有记录
     *
     * @param map 参数map集合
     * @return 返回集合，没有返回空List
     */
    List<CommentsActivityInfo> listByParam(Map<String, Object> map);


	/**
     * 根据主键查询
     *
     * @param commentsId 主键
     * @return 返回记录，没有返回null
     */
	CommentsActivityInfo getById(Integer commentsId);
	
	/**
     * 新增，插入所有字段
     *
     * @param commentsActivityInfo 新增的记录
     * @return 返回影响行数
     */
	int insert(CommentsActivityInfo commentsActivityInfo);
	
	/**
     * 新增，忽略null字段
     *
     * @param commentsActivityInfo 新增的记录
     * @return 返回影响行数
     */
	int insertIgnoreNull(CommentsActivityInfo commentsActivityInfo);
	
	/**
     * 修改，修改所有字段
     *
     * @param commentsActivityInfo 修改的记录
     * @return 返回影响行数
     */
	int update(CommentsActivityInfo commentsActivityInfo);
	
	/**
     * 修改，忽略null字段
     *
     * @param commentsActivityInfo 修改的记录
     * @return 返回影响行数
     */
	int updateIgnoreNull(CommentsActivityInfo commentsActivityInfo);
	
	/**
     * 删除记录
     *
     * @param commentsActivityInfo 待删除的记录
     * @return 返回影响行数
     */
	int delete(CommentsActivityInfo commentsActivityInfo);
	
}