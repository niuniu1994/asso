package com.itSession.cn.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import com.itSession.cn.entity.ActivityInfo;

/**
 * @ClassName: ActivityInfo
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Mapper
public interface ActivityInfoMapper {

    /**
     * 分页查询所有记录
     *
     * @param map 参数map集合
     * @return 返回集合，没有返回空List
     */
    List<ActivityInfo> listByAssociationId(Map<String, Object> map);


	/**
     * 根据主键查询
     *
     * @param activityId 主键
     * @return 返回记录，没有返回null
     */
	ActivityInfo getById(Integer activityId);
	
	/**
     * 新增，插入所有字段
     *
     * @param activityInfo 新增的记录
     * @return 返回影响行数
     */
	int insert(ActivityInfo activityInfo);
	
	/**
     * 新增，忽略null字段
     *
     * @param activityInfo 新增的记录
     * @return 返回影响行数
     */
	int insertIgnoreNull(ActivityInfo activityInfo);
	
	/**
     * 修改，修改所有字段
     *
     * @param activityInfo 修改的记录
     * @return 返回影响行数
     */
	int update(ActivityInfo activityInfo);
	
	/**
     * 修改，忽略null字段
     *
     * @param activityInfo 修改的记录
     * @return 返回影响行数
     */
	int updateIgnoreNull(ActivityInfo activityInfo);
	
	/**
     * 删除记录
     *
     * @param activityInfo 待删除的记录
     * @return 返回影响行数
     */
	int delete(ActivityInfo activityInfo);
	
}