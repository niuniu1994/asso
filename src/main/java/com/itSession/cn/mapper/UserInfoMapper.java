package com.itSession.cn.mapper;

import java.util.List;
import java.util.Map;

import com.itSession.cn.entity.ActivityInfo;
import com.itSession.cn.entity.AssociationInfo;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Mapper;
import com.itSession.cn.entity.UserInfo;
import org.apache.ibatis.annotations.Param;

/**
 * @ClassName: UserInfo
 * @description: 用户
 * @author: hanP
 * @create: 2021-03-11 09:00:37
 */
@Mapper
public interface UserInfoMapper {


	/**
	 * get users by associationId
	 * @param id
	 * @return
	 */
	List<UserInfo> getUsersByAssociationId(Integer id);


	List<UserInfo> getUsersByActivityId(Integer id);

	/**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
	List<UserInfo> listAll();
    
    
    /**
     * 分页查询所有记录
     *
     * @param map 参数map集合
     * @return 返回集合，没有返回空List
     */
    List<UserInfo> listByParam(Map<String, Object> map);


	/**
     * 根据主键查询
     *
     * @param userId 主键
     * @return 返回记录，没有返回null
     */
	UserInfo getById(Integer userId);
	
	/**
     * 新增，插入所有字段
     *
     * @param userInfo 新增的记录
     * @return 返回影响行数
     */
	int insert(UserInfo userInfo);
	
	/**
     * 新增，忽略null字段
     *
     * @param userInfo 新增的记录
     * @return 返回影响行数
     */
	int insertIgnoreNull(UserInfo userInfo);
	
	/**
     * 修改，修改所有字段
     *
     * @param userInfo 修改的记录
     * @return 返回影响行数
     */
	int update(UserInfo userInfo);
	
	/**
     * 修改，忽略null字段
     *
     * @param userInfo 修改的记录
     * @return 返回影响行数
     */
	int updateIgnoreNull(UserInfo userInfo);
	
	/**
     * 删除记录
     *
     * @param userInfo 待删除的记录
     * @return 返回影响行数
     */
	int delete(UserInfo userInfo);

    UserInfo getUserByAccount(@Param("userName") String userName);
}