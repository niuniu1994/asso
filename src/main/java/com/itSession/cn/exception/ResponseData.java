package com.itSession.cn.exception;

import java.util.LinkedHashMap;

/**
 * @program: demo
 * @ClassName: ResponseData
 * @description: 用来返回controller层请求的结果, 默认为三个返回项：code,message,data,如果有其他更多返回项，使用result方法拼接
 * @author: hanP
 * @create: 2020-12-23 17:27
 **/
public class ResponseData extends LinkedHashMap<String, Object> {

    private static final long serialVersionUID = -364546270975223015L;

    public ResponseData result(String key, Object value) {
        this.put(key, value);
        return this;
    }

    public ResponseData success() {
        this.put("resultCode", 200);
        this.put("code", 200);
        this.put("message", "操作成功");
        return this;
    }

    public ResponseData success(Object message) {
        this.put("resultCode", 200);
        this.put("code", 200);
        this.put("message", message);
        return this;
    }

    public ResponseData fail() {
        return this.fail("fail");
    }

    public ResponseData fail(Object message) {
        this.put("resultCode", 400);
        this.put("code", 400);
        this.put("message", message);
        return this;
    }

    public ResponseData fail(int code,Object message) {
        this.put("resultCode", code);
        this.put("code", code);
        this.put("message", message);
        return this;
    }

    public ResponseData redirect(String message) {
        this.put("resultCode", 300);
        this.put("code", 300);
        this.put("message", message);
        return this;
    }

    public ResponseData unauthorized() {
        return this.unauthorized("the current user is unauthorized");
    }

    public ResponseData unauthorized(Object message) {
        this.put("resultCode", 401);
        this.put("code", 402);
        this.put("message", message);
        return this;
    }

    public ResponseData forbidden(Object message) {
        this.put("resultCode", 403);
        this.put("code", 403);
        this.put("message", message);
        return this;
    }

    public ResponseData code(int code) {
        return result("code", code);
    }

    public ResponseData message(String message) {
        return result("message", message);
    }

    public ResponseData data(Object data) {
        return result("data", data);
    }

    public ResponseData count(int count) {
        return result("count", count);
    }

}
