package com.itSession.cn.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: demo
 * @ClassName: GlobalExceptionHandler
 * @description: 自定义全局异常响应（以保证controller层抛出的异常能够全局响应JSON）
 *  @ControllerAdvice是一个@Component，用于定义@ExceptionHandler
 * @author: hanP
 * @create: 2020-12-23 17:26
 **/

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = MyException.class)
    @ResponseBody
    public ResponseData jsonExceptionHandler(HttpServletRequest request, MyException e) {
        return new ResponseData().fail(e.getMessage());
    }


}
