/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 8.0.21 : Database - disease-db
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`disease-db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `disease-db`;

/*Table structure for table `sj_activity_info` */

DROP TABLE IF EXISTS `sj_activity_info`;

CREATE TABLE `sj_activity_info` (
  `activity_id` int NOT NULL AUTO_INCREMENT COMMENT '主键编号',
  `activity_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '活动名称',
  `activity_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '活动介绍',
  `activity_start_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '开始时间',
  `activity_end_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '结束时间',
  `activity_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '举办地点',
  `activity_association_id` int DEFAULT NULL COMMENT '协会id',
  `activity_association_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '协会名称',
  `activity_school` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '学校名称',
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `sj_activity_info` */

insert  into `sj_activity_info`(`activity_id`,`activity_name`,`activity_content`,`activity_start_time`,`activity_end_time`,`activity_address`,`activity_association_id`,`activity_association_name`,`activity_school`) values (1,'羽毛球比赛','羽毛球比赛羽毛球比赛','202-02-25 12:12:45','202-02-25 14:12:45','羽毛球比赛场馆',1,'羽毛球协会',NULL),(2,'羽毛球比赛','羽毛球比赛','202-02-25 12:12:45','202-02-25 14:12:45','羽毛球比赛场馆',1,'羽毛球协会',NULL),(3,'篮球比赛','篮球比赛','202-02-25 12:12:45','202-02-25 14:12:45','篮球比赛场馆',2,'篮球比赛',NULL);

/*Table structure for table `sj_association_info` */

DROP TABLE IF EXISTS `sj_association_info`;

CREATE TABLE `sj_association_info` (
  `association_id` int NOT NULL AUTO_INCREMENT COMMENT '主键编号',
  `association_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '协会名称',
  `association_content` varchar(999) COLLATE utf8_bin DEFAULT NULL COMMENT '协会介绍',
  `association_img` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '协会照片',
  `association_address` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '协会地址',
  `association_status` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '状态：1.申请中  2.同意  3.拒绝  4.删除',
  PRIMARY KEY (`association_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `sj_association_info` */

insert  into `sj_association_info`(`association_id`,`association_name`,`association_content`,`association_img`,`association_address`,`association_status`) values (1,'羽毛球协会','羽毛球协会','/2021/2/1614230047576.jpg','羽毛球协会羽毛球协会羽毛球协会',NULL),(2,'篮球比赛','篮球比赛','/2021/2/1614233411906.png','篮球比赛场馆',NULL);

/*Table structure for table `sj_comments_activity_info` */

DROP TABLE IF EXISTS `sj_comments_activity_info`;

CREATE TABLE `sj_comments_activity_info` (
  `comments_id` int NOT NULL AUTO_INCREMENT COMMENT '主键标号',
  `comments_user_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '评论用户',
  `comments_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '用户姓名',
  `comments_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '评论内容',
  `comments_create_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '评论时间',
  `comments_activity_id` int DEFAULT NULL COMMENT '活动id',
  PRIMARY KEY (`comments_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='评论活动';

/*Data for the table `sj_comments_activity_info` */

/*Table structure for table `sj_comments_association_info` */

DROP TABLE IF EXISTS `sj_comments_association_info`;

CREATE TABLE `sj_comments_association_info` (
  `comments_id` int NOT NULL AUTO_INCREMENT COMMENT '主键标号',
  `comments_user_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '评论用户',
  `comments_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '用户姓名',
  `comments_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '评论内容',
  `comments_create_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '评论时间',
  `comments_association_id` int DEFAULT NULL COMMENT '协会id',
  PRIMARY KEY (`comments_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='评论协会';

/*Data for the table `sj_comments_association_info` */

/*Table structure for table `sj_user_activity_info` */

DROP TABLE IF EXISTS `sj_user_activity_info`;

CREATE TABLE `sj_user_activity_info` (
  `user_activity_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_activity_user_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '账号',
  `user_activity_user_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '姓名',
  `user_activity_activity_id` int DEFAULT NULL COMMENT '活动id',
  `user_activity_activity_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '活动名称',
  `user_activity_ststue` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '状态 1申请中  2.同意  3.拒绝',
  PRIMARY KEY (`user_activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户活动';

/*Data for the table `sj_user_activity_info` */

/*Table structure for table `sj_user_association_info` */

DROP TABLE IF EXISTS `sj_user_association_info`;

CREATE TABLE `sj_user_association_info` (
  `user_association_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_association_user_account` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '账号',
  `user_association_user_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '名称',
  `user_association_association_id` int DEFAULT NULL COMMENT '协会id',
  `user_association_association_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '协会名称',
  `user_association_status` int DEFAULT NULL COMMENT '状态 1申请中  2.同意  3.拒绝',
  PRIMARY KEY (`user_association_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户协会';

/*Data for the table `sj_user_association_info` */

insert  into `sj_user_association_info`(`user_association_id`,`user_association_user_account`,`user_association_user_name`,`user_association_association_id`,`user_association_association_name`,`user_association_status`) values (1,'114','用户1',1,'羽毛球协会',2);

/*Table structure for table `sj_user_info` */

DROP TABLE IF EXISTS `sj_user_info`;

CREATE TABLE `sj_user_info` (
  `user_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_account` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '账号',
  `user_pwd` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '密码',
  `user_img` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '头像',
  `user_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '姓名',
  `user_association_id` int DEFAULT NULL COMMENT '协会id',
  `user_phone` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '手机号',
  `user_email` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '邮箱',
  `user_school` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '学校',
  `user_role` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '角色 1.管理员  2.普通用户',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户';

/*Data for the table `sj_user_info` */

insert  into `sj_user_info`(`user_id`,`user_account`,`user_pwd`,`user_img`,`user_name`,`user_association_id`,`user_phone`,`user_email`,`user_school`,`user_role`) values (1,'114','123456','/2021/2/1614233149606.png','用户1',NULL,NULL,NULL,NULL,NULL),(2,'115','123456','/2021/2/1614233140690.jpg','用户2',NULL,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;





alter table sj_user_info
    add constraint sj_admin_sj_association_info_association_id_fk
        foreign key (user_association_id) references sj_association_info (association_id);

CREATE TABLE `sj_user_association` (
                                `ua_id` int AUTO_INCREMENT '主键',
                                `user_id` int NOT NULL  COMMENT 'user id',
                                `user_association_id` int DEFAULT NULL COMMENT '协会id',
                                PRIMARY KEY (`ua_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='user&association';