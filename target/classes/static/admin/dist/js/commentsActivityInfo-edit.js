var editorD;

$(function () {

    //富文本编辑器 用于景点详情编辑
    const E = window.wangEditor;
    editorD = new E('#wangEditor')
    // 设置编辑区域高度为 750px
    editorD.config.height = 750
    //配置服务端图片上传地址
    editorD.config.uploadImgServer = '/admin/upload/files'
    editorD.config.uploadFileName = 'files'
    //限制图片大小 2M
    editorD.config.uploadImgMaxSize = 2 * 1024 * 1024
    //限制一次最多能传几张图片 一次最多上传 5 个图片
    editorD.config.uploadImgMaxLength = 5
    //隐藏插入网络图片的功能
    editorD.config.showLinkImg = false
    editorD.config.uploadImgHooks = {
        // 图片上传并返回了结果，图片插入已成功
        success: function (xhr) {
            console.log('success', xhr)
        },
        // 图片上传并返回了结果，但图片插入时出错了
        fail: function (xhr, editor, resData) {
            console.log('fail', resData)
        },
        // 上传图片出错，一般为 http 请求的错误
        error: function (xhr, editor, resData) {
            console.log('error', xhr, resData)
        },
        // 上传图片超时
        timeout: function (xhr) {
            console.log('timeout')
        },
        customInsert: function (insertImgFn, result) {
            if (result != null && result.resultCode == 200) {
                // insertImgFn 可把图片插入到编辑器，传入图片 src ，执行函数即可
                result.data.forEach(img => {
                    insertImgFn(img)
                });
            } else {
                alert("error");
            }
        }
    }
    editorD.create();

    //图片上传插件初始化 用于景点预览图上传
    new AjaxUpload('#uploadGoodsCoverImg', {
        action: '/public/photoUpload',
        name: 'file',
        autoSubmit: true,
        responseType: "json",
        onSubmit: function (file, extension) {
            if (!(extension && /^(jpg|jpeg|png|gif)$/.test(extension.toLowerCase()))) {
                alert('只支持jpg、png、gif格式的文件！');
                return false;
            }
        },
        onComplete: function (file, r) {
            if (r != null) {
                $("#attractionsImg").attr("src", baseUrlProject + "/public/getImg?path=" + r.data);
                $("#attractionsImg").attr("style", "width: 128px;height: 128px;display:block;");
                return false;
            } else {
                alert("error");
            }
        }
    });
});

$('#saveButton').click(function () {

    var commentsId = $("#commentsId").val();
    if (isNull(commentsId)) {
        swal("请输入 主键标号 ", {
            icon: "error",
        });
        return;
    }
    var commentsUserAccount = $("#commentsUserAccount").val();
    if (isNull(commentsUserAccount)) {
        swal("请输入 评论用户 ", {
            icon: "error",
        });
        return;
    }
    var commentsUserName = $("#commentsUserName").val();
    if (isNull(commentsUserName)) {
        swal("请输入 用户姓名 ", {
            icon: "error",
        });
        return;
    }
    var commentsContent = $("#commentsContent").val();
    if (isNull(commentsContent)) {
        swal("请输入 评论内容 ", {
            icon: "error",
        });
        return;
    }
    var commentsCreateDate = $("#commentsCreateDate").val();
    if (isNull(commentsCreateDate)) {
        swal("请输入 评论时间 ", {
            icon: "error",
        });
        return;
    }
    var commentsActivityId = $("#commentsActivityId").val();
    if (isNull(commentsActivityId)) {
        swal("请输入 活动id ", {
            icon: "error",
        });
        return;
    }

    var attractionsImg = $('#attractionsImg')[0].src.split('?path=')[1];
    if (isNull(attractionsImg) || attractionsImg.indexOf('img-upload') != -1) {
        swal("封面图片不能为空", {
            icon: "error",
        });
        return;
    }
    var url = '/commentsActivityInfo/insertIgnoreNull';
    var swlMessage = '保存成功';
    var data = {
        "commentsUserAccount": commentsUserAccount
        , "commentsUserName": commentsUserName
        , "commentsContent": commentsContent
        , "commentsCreateDate": commentsCreateDate
        , "commentsActivityId": commentsActivityId
    };
    if (commentsId > 0) {
        url = '/commentsActivityInfo/updateIgnoreNull';
        swlMessage = '修改成功';
        data = {
            "commentsId": commentsId,
            "commentsUserAccount": commentsUserAccount
            , "commentsUserName": commentsUserName
            , "commentsContent": commentsContent
            , "commentsCreateDate": commentsCreateDate
            , "commentsActivityId": commentsActivityId
        };
    }
    console.log(data);
    $.ajax({
        type: 'POST',//方法类型
        url: url,
        data: data,
        success: function (result) {
            if (result.resultCode == 200) {
                $('#goodsModal').modal('hide');
                swal({
                    title: swlMessage,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#1baeae',
                    confirmButtonText: '返回列表',
                    confirmButtonClass: 'btn btn-success',
                    buttonsStyling: false
                }).then(function () {
                    window.location.href = "/admin/commentsActivityInfo";
                })
            } else {
                $('#goodsModal').modal('hide');
                swal(result.message, {
                    icon: "error",
                });
            }
            ;
        },
        error: function () {
            swal("操作失败", {
                icon: "error",
            });
        }
    });
});

$('#cancelButton').click(function () {
    window.location.href = "/admin/commentsActivityInfo";
});


