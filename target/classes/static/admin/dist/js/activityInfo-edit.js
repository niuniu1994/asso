var editorD;

$(function () {

    //富文本编辑器 用于景点详情编辑
    const E = window.wangEditor;
    editorD = new E('#wangEditor')
    // 设置编辑区域高度为 750px
    editorD.config.height = 750
    //配置服务端图片上传地址
    editorD.config.uploadImgServer = '/admin/upload/files'
    editorD.config.uploadFileName = 'files'
    //限制图片大小 2M
    editorD.config.uploadImgMaxSize = 2 * 1024 * 1024
    //限制一次最多能传几张图片 一次最多上传 5 个图片
    editorD.config.uploadImgMaxLength = 5
    //隐藏插入网络图片的功能
    editorD.config.showLinkImg = false
    editorD.config.uploadImgHooks = {
        // 图片上传并返回了结果，图片插入已成功
        success: function (xhr) {
            console.log('success', xhr)
        },
        // 图片上传并返回了结果，但图片插入时出错了
        fail: function (xhr, editor, resData) {
            console.log('fail', resData)
        },
        // 上传图片出错，一般为 http 请求的错误
        error: function (xhr, editor, resData) {
            console.log('error', xhr, resData)
        },
        // 上传图片超时
        timeout: function (xhr) {
            console.log('timeout')
        },
        customInsert: function (insertImgFn, result) {
            if (result != null && result.resultCode == 200) {
                // insertImgFn 可把图片插入到编辑器，传入图片 src ，执行函数即可
                result.data.forEach(img => {
                    insertImgFn(img)
                });
            } else {
                alert("error");
            }
        }
    }
    editorD.create();

});

$('#saveButton').click(function () {

    var activityId = $("#activityId").val();
    var activityName = $("#activityName").val();
    if (isNull(activityName)) {
        swal("请输入 活动名称 ", {
            icon: "error",
        });
        return;
    }
    var activityContent = $("#activityContent").val();
    if (isNull(activityContent)) {
        swal("请输入 活动介绍 ", {
            icon: "error",
        });
        return;
    }
    var activityStartTime = $("#activityStartTime").val();
    if (isNull(activityStartTime)) {
        swal("请输入 开始时间 ", {
            icon: "error",
        });
        return;
    }
    var activityEndTime = $("#activityEndTime").val();
    if (isNull(activityEndTime)) {
        swal("请输入 结束时间 ", {
            icon: "error",
        });
        return;
    }
    var activityAddress = $("#activityAddress").val();
    if (isNull(activityAddress)) {
        swal("请输入 举办地点 ", {
            icon: "error",
        });
        return;
    }
    var activityAssociationId = $("#activityAssociationId").val();
    if (isNull(activityAssociationId)) {
        swal("请输入 协会id ", {
            icon: "error",
        });
        return;
    }
    var activityAssociationName = $("#activityAssociationName").val();
    if (isNull(activityAssociationName)) {
        swal("请输入 协会名称 ", {
            icon: "error",
        });
        return;
    }
    var activitySchool = $("#activitySchool").val();
    if (isNull(activitySchool)) {
        swal("请输入 学校名称 ", {
            icon: "error",
        });
        return;
    }

    var url = '/activityInfo/insertIgnoreNull';
    var swlMessage = '保存成功';
    var data = {
        "activityName": activityName
        , "activityContent": activityContent
        , "activityStartTime": activityStartTime
        , "activityEndTime": activityEndTime
        , "activityAddress": activityAddress
        , "activityAssociationId": activityAssociationId
        , "activityAssociationName": activityAssociationName
        , "activitySchool": activitySchool
    };
    if (activityId > 0) {
        url = '/activityInfo/updateIgnoreNull';
        swlMessage = '修改成功';
        data = {
            "activityId": activityId,
            "activityName": activityName
            , "activityContent": activityContent
            , "activityStartTime": activityStartTime
            , "activityEndTime": activityEndTime
            , "activityAddress": activityAddress
            , "activityAssociationId": activityAssociationId
            , "activityAssociationName": activityAssociationName
            , "activitySchool": activitySchool
        };
    }
    console.log(data);
    $.ajax({
        type: 'POST',//方法类型
        url: url,
        data: data,
        success: function (result) {
            if (result.resultCode == 200) {
                $('#goodsModal').modal('hide');
                swal({
                    title: swlMessage,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#1baeae',
                    confirmButtonText: '返回列表',
                    confirmButtonClass: 'btn btn-success',
                    buttonsStyling: false
                }).then(function () {
                    window.location.href = "/admin/activityInfo";
                })
            } else {
                $('#goodsModal').modal('hide');
                swal(result.message, {
                    icon: "error",
                });
            }
            ;
        },
        error: function () {
            swal("操作失败", {
                icon: "error",
            });
        }
    });
});

$('#cancelButton').click(function () {
    window.location.href = "/admin/activityInfo";
});


