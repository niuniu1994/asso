$(function () {
    let index = window.location.href.lastIndexOf('/')
    let activityId = window.location.href.substring(index + 1);
    let url = null

    if (isNaN(activityId)){
        url = '/userInfo/association'
    }else {
        url = '/userInfo/activity/' + activityId
    }

    $("#jqGrid").jqGrid({
        url: url,
        datatype: "json",
        colModel: [
            {label: '编号', name: "userId", index: "userId", width: 40, key: true}
            , {label: "账号", name: "userAccount", index: "userAccount", width: 60}
            , {label: "密码", name: "userPwd", index: "userPwd", width: 60}
            , {label: "头像", name: "userImg", index: "userImg", width: 60, formatter: coverImageFormatter}
            , {label: "姓名", name: "userName", index: "userName", width: 60}
            , {label: "协会id", name: "userAssociationId", index: "userAssociationId", width: 60}
            , {label: "手机号", name: "userPhone", index: "userPhone", width: 60}
            , {label: "邮箱", name: "userEmail", index: "userEmail", width: 60}
            , {label: "学校", name: "userSchool", index: "userSchool", width: 60}
            , {label: "角色 1.管理员  2.普通用户", name: "userRole", index: "userRole", width: 60}
        ],
        loadComplete: function (data) {
            console.log(data);
            var countPage = data.count;
            console.log(countPage);
            console.log(getResult());

        },
        height: 760,
        rowNum: 20,
        rowList: [20, 50, 80],
        styleUI: 'Bootstrap',
        loadtext: '信息读取中...',
        rownumbers: false,
        rownumWidth: 20,
        rowheight: 5,
        autowidth: true,
        multiselect: true,
        viewrecords: true,
        pager: "#jqGridPager",
        jsonReader: {
            root: "data",
            page: "1",
            total: "1",
            records: "1"
        },
        prmNames: {
            page: "page",
            rows: "limit",
            order: "order",
        },
        gridComplete: function () {
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "hidden"});
        }
    });

    $(window).resize(function () {
        $("#jqGrid").setGridWidth($(".card-body").width());
    });


    function coverImageFormatter(cellvalue) {
        cellvalue = baseUrlProject + "/public/getImg?path=" + cellvalue;
        return "<img src='" + cellvalue + "' height=\"100\" width=\"80\" alt='头像'/>";
    }

});

//获取结果结合的函数，可以通过此函数获取查询后匹配的所有数据行。
function getResult() {
    var o = jQuery("#jqGrid");
    //获取当前显示的记录
    /* var rows = o.jqGrid('getRowData');
     console.log(rows)*/

    //获取显示配置记录数量
    var rowNum = o.jqGrid('getGridParam', 'rowNum');
    //获取查询得到的总记录数量
    //var total = o.jqGrid('getGridParam', 'records');
    //设置rowNum为总记录数量并且刷新jqGrid，使所有记录现出来调用getRowData方法才能获取到所有数据
    //o.jqGrid('setGridParam', {rowNum: total}).trigger('reloadGrid');
    //输出所有匹配的
    //var rows = o.jqGrid('getRowData');
    //o.jqGrid('setGridParam', {rowNum: rowNum}).trigger('reloadGrid'); //还原原来显示的记录数量
    return rowNum;
}

/**
 * jqGrid重新加载
 */
function reload() {
    var page = $("#jqGrid").jqGrid('getGridParam', 'page');
    $("#jqGrid").jqGrid('setGridParam', {
        page: page
    }).trigger("reloadGrid");
}

/**
 * 搜索功能
 */
function search() {
    //标题关键字
    var keyword = $('#keyword').val();
    if (!validLength(keyword, 20)) {
        swal("搜索字段长度过大!", {
            icon: "error",
        });
        return false;
    }
    //数据封装
    var searchData = {"keyword": keyword};
    //传入查询条件参数
    $("#jqGrid").jqGrid("setGridParam", {postData: searchData});
    //点击搜索按钮默认都从第一页开始
    $("#jqGrid").jqGrid("setGridParam", {page: 1});
    //提交post并刷新表格
    $("#jqGrid").jqGrid("setGridParam", {url: '/userInfo/listByParam'}).trigger("reloadGrid");
}

/**
 * 添加
 */
function addGoods() {
    window.location.href = "/admin/userInfo/edit";
}

/**
 * 修改
 */
function editGoods() {
    var id = getSelectedRow();
    if (id == null) {
        return;
    }
    window.location.href = "/admin/userInfo/edit/" + id;
}

/**
 * 删除
 */
function deleteLink() {
    var id = getSelectedRow();
    if (id == null) {
        return;
    }
    swal({
        title: "确认弹框",
        text: "确认要删除数据吗?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((flag) => {
            if (flag) {
                $.ajax({
                    type: "POST",
                    url: "/userInfo/delete",
                    data: {"userId": id},
                    success: function (r) {
                        if (r.resultCode == 200) {
                            swal("删除成功", {
                                icon: "success",
                            });
                            $("#jqGrid").trigger("reloadGrid");
                        } else {
                            swal(r.message, {
                                icon: "error",
                            });
                        }
                    }
                });
            }
        }
    );
}

